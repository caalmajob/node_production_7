const isAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
        next();
    } else {
        return res.redirect('/auth/login');
    }
}

const isAdmin = (req, res, next) => {
    if (req.isAuthenticated()) {
        if(req.user.role === 'admin') {
             next();
        } else {
            //Está logeado pero no es admin
            const error = new Error('No tienes permisos de administrador');
            error.status = 403;
           return next(error);
        }
    } else {
        return res.redirect('/auth/login');
    }
}

module.exports = { isAuthenticated, isAdmin};