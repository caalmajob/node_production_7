const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const petSchema = new Schema( {
    name: {type: String, required: true},
    age: {type: Number},
    breed: {type: String},
    color: {type: String, required: true},
    specie: {type: String, required: true},
    image: {type: String }
}, {
    timestamps: true,
});

const Pet = mongoose.model('Pet', petSchema);

module.exports = Pet;